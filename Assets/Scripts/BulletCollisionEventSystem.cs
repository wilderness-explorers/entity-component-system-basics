﻿using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;

//To ensure this runs after the last frame of the physics system.
[UpdateAfter(typeof(EndFramePhysicsSystem))]
public class BulletCollisionEventSystem : JobComponentSystem
{
    BuildPhysicsWorld m_buildPhysicsWorldSystem;

    //Time interval between successive physics iteration steps.
    StepPhysicsWorld m_stepPhysicsWorldSystem;

    protected override void OnCreate()
    {
        //Initializing physics.
        m_buildPhysicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
        m_stepPhysicsWorldSystem = World.GetOrCreateSystem<StepPhysicsWorld>();
    }

    //To execute the actual job - collision event.
    struct CollisionEventImpulseJob: ICollisionEventsJob
    {
        //Getting bullet data component and storing it in a "BulletGroup". Read only because there is a need to only read the data. Optimizes memory too.
        [ReadOnly] public ComponentDataFromEntity<BulletData> BulletGroup;
        public ComponentDataFromEntity<VirusData> VirusGroup;

        //Similar to content inside "Foreach" loops.
        public void Execute(CollisionEvent collisionEvent)
        {
            //Generic entityA and entityB. Any 2 objects that might collide with each other.
            Entity entityA = collisionEvent.Entities.EntityA;
            Entity entityB = collisionEvent.Entities.EntityB;

            //To find which entity is which.
            //Could be 2 viruses colliding with each other - which can be ignored.
            bool isTargetA = VirusGroup.Exists(entityA);
            bool isTargetB = VirusGroup.Exists(entityB);

            bool isBulletA = BulletGroup.Exists(entityA);
            bool isBulletB = BulletGroup.Exists(entityB);

            //Collision to consider - bullet with a virus.
            if(isBulletA && isTargetB)
            {
                //Getting hold of the "alive" data from virus entity.
                var aliveComponent = VirusGroup[entityB];
                aliveComponent.alive = false;

                //After setting the calue, putting it back to the entity.
                VirusGroup[entityB] = aliveComponent;
            }

            if (isBulletB && isTargetA)
            {
                //Getting hold of the "alive" data from virus entity.
                var aliveComponent = VirusGroup[entityA];
                aliveComponent.alive = false;

                //After setting the calue, putting it back to the entity.
                VirusGroup[entityA] = aliveComponent;
            }
        }
    }

    //Contains "OnUpdate" which will contain the job. This is parallel computing.
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        JobHandle jobHandle = new CollisionEventImpulseJob
        {
            BulletGroup = GetComponentDataFromEntity<BulletData>(),
            VirusGroup = GetComponentDataFromEntity<VirusData>()
        }.Schedule(m_stepPhysicsWorldSystem.Simulation, ref m_buildPhysicsWorldSystem.PhysicsWorld, inputDeps);

        jobHandle.Complete();
        return jobHandle;
    }
}
