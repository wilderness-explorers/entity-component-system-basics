﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Collections;
using Unity.Physics;

public class TimedDestroySystem : JobComponentSystem
{
    //Contains "OnUpdate" which will contain the job. This is parallel computing.
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float dT = Time.DeltaTime;

        //no job handle is assigned, as we are destroying objects. Structurally changing the memory. Cannot execute parallel jobs and destroy objects at the same time.
        Entities
            .WithoutBurst().WithStructuralChanges()
            .ForEach((Entity entity,
            ref Translation position,
            ref LifetimeData lifetimeData) =>
            {
                //Count down delta time from lifetime value.
                lifetimeData.lifeTime -= dT;
                if(lifetimeData.lifeTime <= 0.0f)
                {
                    EntityManager.DestroyEntity(entity);
                }
            }).Run();

        //Instantiate a bunch of WBCs at position where virus gets destroyed.
        Entities
            .WithoutBurst().WithStructuralChanges()
            .ForEach((Entity entity,
            ref Translation position,
            ref VirusData virusData) =>
            {
                //Destroy virus entity depending on it's bool data.
                if (!virusData.alive)
                {
                    for (int i = 0; i < 100; i++)
                    {
                        float3 offset = (float3)UnityEngine.Random.insideUnitSphere * 2.0f;
                        var splat = ECSManager.manager.Instantiate(ECSManager.wbc);

                        //Assign direction.
                        float3 randomDir = new float3(UnityEngine.Random.Range(-1, 1), UnityEngine.Random.Range(-1, 1), UnityEngine.Random.Range(-1, 1));

                        //Add offset to the position of virus.
                        ECSManager.manager.SetComponentData(splat, new Translation { Value = position.Value + offset });

                        //"2": like an extra push. Speed at which, wbc will splatter away from spawn point.
                        ECSManager.manager.SetComponentData(splat, new PhysicsVelocity { Linear = randomDir * 2 });
                    }

                    EntityManager.DestroyEntity(entity);
                }
            }).Run();

        return inputDeps;
    }
}
