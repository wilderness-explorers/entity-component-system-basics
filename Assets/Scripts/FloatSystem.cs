﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Collections;
using Unity.Physics;

public class FloatSystem : JobComponentSystem
{
    //Contains "OnUpdate" which will contain the job. This is parallel computing.
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float dT = Time.DeltaTime;

        //"Schedule" indicates that jobhandle cannot run until inputDeps has finished. InputDeps doesnt really exist, so jobhandle runs anyway.
        //Foreach is like a loop, but a parallel loop. Can process a whole bunch at same time. Uses multicore functionality of the computer.
        //Foreach is going to loop through every entity, that has the 4 components mentioned, attached to it.
        var jobHandle = Entities
            .WithName("FloatSystem")
            .ForEach((ref PhysicsVelocity physics,
            ref Translation position,
            ref Rotation rotation,
            ref FloatData floatData) =>
            {
                float s = math.sin((dT + position.Value.x) * 0.5f) * floatData.speed;
                float c = math.cos((dT + position.Value.y) * 0.5f) * floatData.speed;

                float3 dir = new float3(s, c, s);
                physics.Linear += dir;
            }).Schedule(inputDeps);

        //Job gets scheduled in the system. Without ".Complete", job will be executed again, before the previous job is complete.
        jobHandle.Complete();

        //To create a job.
        //Job system content, should be the entity version of all the maths, transforms, etc.


        return jobHandle;
    }
}
