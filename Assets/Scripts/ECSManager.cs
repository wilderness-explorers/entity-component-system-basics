﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class ECSManager : MonoBehaviour
{
    //Entity manager allows us to spawn things in to the entity system & check if something exists. Similar to pool system.
    public static EntityManager manager;

    public GameObject virusPrefab;
    public GameObject rbcPrefab;
    public GameObject wbcPrefab;
    public GameObject bulletPrefab;
    public GameObject player;

    int numVirus = 500;
    int numRbc = 500;
    int numBullet = 10;

    //Binary large asset store. Viruses will be created within it.
    //This is a "native" object. All native objects needs to destroyed later on.
    BlobAssetStore store;

    Entity bullet;
    public static Entity wbc;

    void Start()
    {
        store = new BlobAssetStore();
        manager = World.DefaultGameObjectInjectionWorld.EntityManager;

        //Settings tell the manager, how to create an object or convert object into an entity.
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, store);

        //Creating the entity.
        Entity virus = GameObjectConversionUtility.ConvertGameObjectHierarchy(virusPrefab, settings);
        Entity rbc = GameObjectConversionUtility.ConvertGameObjectHierarchy(rbcPrefab, settings);
        bullet = GameObjectConversionUtility.ConvertGameObjectHierarchy(bulletPrefab, settings);
        wbc = GameObjectConversionUtility.ConvertGameObjectHierarchy(wbcPrefab, settings);

        //To create a bunch of entities and store in the entities system.
        for (int i = 0; i < numVirus; i++)
        {
            var instance = manager.Instantiate(virus);

            //To position the prefab, randomly around (0,0,0).
            float x = UnityEngine.Random.Range(-50, 50);
            float y = UnityEngine.Random.Range(-50, 50);
            float z = UnityEngine.Random.Range(-50, 50);

            //Entity system has float3 instead of a vector3.
            float3 position = new float3(x, y, z);

            float randomSpeed = UnityEngine.Random.Range(0, 10) / 10.0f;

            //To give postion to entity - little different - as entity system separates all the componenets from the actual entity.
            manager.SetComponentData(instance, new Translation { Value = position });
            manager.SetComponentData(instance, new FloatData { speed = randomSpeed });
        }

        for (int i = 0; i < numRbc; i++)
        {
            var instance = manager.Instantiate(rbc);

            //To position the prefab, randomly around (0,0,0).
            float x = UnityEngine.Random.Range(-50, 50);
            float y = UnityEngine.Random.Range(-50, 50);
            float z = UnityEngine.Random.Range(-50, 50);

            //Entity system has float3 instead of a vector3.
            float3 position = new float3(x, y, z);

            float randomSpeed = UnityEngine.Random.Range(0, 10) / 10.0f;

            //To give postion to entity - little different - as entity system separates all the componenets from the actual entity.
            manager.SetComponentData(instance, new Translation { Value = position });
            manager.SetComponentData(instance, new FloatData { speed = randomSpeed });
        }
    }

    void Update()
    {
        //Mixing monobehaviour code with ECS coding.
        if(Input.GetMouseButtonDown(0))
        {
            for(int i = 0; i < numBullet; i++)
            {
                var instance = manager.Instantiate(bullet);
                var startPos = player.transform.position + UnityEngine.Random.insideUnitSphere * 2;

                //Instantiate bullets at right position and orientation, then give forward motion to bullet.
                manager.SetComponentData(instance, new Translation { Value = startPos });
                manager.SetComponentData(instance, new Rotation { Value = player.transform.rotation });
            }
        }
    }

    private void OnDestroy()
    {
        store.Dispose();
    }
}